## Frontend test kazapronta

### Goals

+ Login form with authentication and responsive.
+ Two rows product showcase with 6 items, 3 items in each row, also responsive.
+ Each product must contain its respectly Title, category, image and price.

### Inspirations for the showcase:

New CSS techniques, involving `grid`, `flexbox` and semantic HTML.

Some articles to follow:

[The Truth About Multiple H! Tags in the HTML5 Era](https://webdesign.tutsplus.com/articles/the-truth-about-multiple-h1-tags-in-the-html5-era--webdesign-16824)

[We Write CSS Like We Did in the 90s, and Yes, It’s Silly](https://alistapart.com/article/we-write-css-like-we-did-in-the-90s-and-yes-its-silly)

[MDN CSS grid](https://developer.mozilla.org/en-US/docs/Web/CSS/grid)

### Inspirations for the login page:

Yes, there's a lot of JS libraries for validation, but in the recent years HTML has progressed and came up with the `pattern` attribute.

While I could go on and insert one, two JS libraries to validate the input data, I relied on the attribute aforementioned.
Simple, elegant and semantic HTML.

Some articles to read:

[The Input (Form Input) element - MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)

[Vertical Centering - Solved by Flexbox](https://philipwalton.github.io/solved-by-flexbox/demos/vertical-centering/)
